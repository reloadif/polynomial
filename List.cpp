#include "List.h"

List::List() : startChain(new ListChain(0, 0, 0, -1)), realElements(0) {
    startChain->setNextChain(startChain);
}
List::~List() {
    ListChain* previosChain = startChain;
    ListChain* removableChain = startChain->getNextChain();

    while (removableChain != startChain) {
        previosChain = removableChain;
        removableChain = removableChain->getNextChain();

        delete previosChain;
    }

    delete startChain;
}

MonomStruct List::removeFromList(const int index) {
    MonomStruct resultMonom(0, 0, 0, -1);

    if (index >= 0 && index < realElements) {
        ListChain* previosChain = startChain;
        ListChain* removableChain = startChain;

        for (int i = 0; i <= index; i++) {
            previosChain = removableChain;
            removableChain = removableChain->getNextChain();
        }
        previosChain->setNextChain(removableChain->getNextChain());

        resultMonom = removableChain->getMonomStruct();

        realElements -= 1;
        delete removableChain;
    }

    return resultMonom;
}
void List::addToList(const MonomStruct& monom) {
    ListChain* previosChain;
    ListChain* currentChain;

    bool flag;

    previosChain = searchInList(monom.compoundDegree, flag);

    if (flag) {
        currentChain = previosChain->getNextChain();
        currentChain->getMonomStruct().factor += monom.factor;
        if (!currentChain->getMonomStruct().factor) {
            previosChain->setNextChain( currentChain->getNextChain() );

            realElements--;
            delete currentChain;
        }
    }
    else {
        currentChain = new ListChain(monom);
        currentChain->setNextChain(previosChain->getNextChain());
        previosChain->setNextChain(currentChain);

        realElements += 1;
    }
}

ListChain* List::searchInList(const int compoundDegree, bool& isFind) {
    isFind = false;

    ListChain* previosChain = startChain;
    ListChain* searchChain = previosChain->getNextChain();

    do {

        if (searchChain->getMonomStruct().compoundDegree == compoundDegree) {
            isFind = true;
            break;
        }
        else if (searchChain->getMonomStruct().compoundDegree < compoundDegree) {
            break;
        }
        else if (searchChain->getMonomStruct().compoundDegree > compoundDegree) {
            previosChain = searchChain;
            searchChain = searchChain->getNextChain();
        }

    } while (searchChain != startChain);

    return previosChain;
}

int List::getSizeList() {
    return realElements;
}
bool List::isEmpty() {
    return !realElements;
}

MonomStruct List::getMonomStruct(const int index) {
    MonomStruct resultMonom = startChain->getMonomStruct();

    if (index >= 0 && index < realElements) {
        ListChain* currentChain = startChain;

        for (int i = 0; i <= index; i++) {
            currentChain = currentChain->getNextChain();
        }

        resultMonom = currentChain->getMonomStruct();
    }

    return resultMonom;
}

void List::sumOfLists(List& list) {
    ListChain* previosChain1 = this->startChain;
    ListChain* previosChain2 = list.startChain;

    ListChain* currentChain1 = previosChain1->getNextChain();
    ListChain* currentChain2 = previosChain2->getNextChain();

    while (list.startChain != currentChain2) {

        if (currentChain1->getMonomStruct().compoundDegree > currentChain2->getMonomStruct().compoundDegree) {
            currentChain1 = currentChain1->getNextChain();
            previosChain1 = previosChain1->getNextChain();
        }
        else if (currentChain1->getMonomStruct().compoundDegree < currentChain2->getMonomStruct().compoundDegree) {
            previosChain1->setNextChain(currentChain2);
            previosChain2->setNextChain(currentChain2->getNextChain());

            currentChain2->setNextChain(currentChain1);
            previosChain1 = currentChain2;
            currentChain2 = previosChain2->getNextChain();

            realElements++;
            list.realElements--;
        }
        else {
            if ( currentChain1->getMonomStruct().factor + currentChain2->getMonomStruct().factor ) {
                currentChain1->getMonomStruct().factor += currentChain2->getMonomStruct().factor;
            }
            else {
                previosChain1->setNextChain( currentChain1->getNextChain() );

                realElements -= 1;
                delete currentChain1;
            }

            currentChain1 = currentChain1->getNextChain();
            previosChain1 = previosChain1->getNextChain();

            currentChain2 = currentChain2->getNextChain();
            previosChain2 = previosChain2->getNextChain();
        }

    }

}

List List::operator+(List& list) {
    List resultList;

    ListChain* currentChain1 = this->startChain->getNextChain();
    ListChain* currentChain2 = list.startChain->getNextChain();
    ListChain* currentChain3 = resultList.startChain;

    while (this->startChain != currentChain1 || list.startChain != currentChain2) {

         if (currentChain1->getMonomStruct().compoundDegree > currentChain2->getMonomStruct().compoundDegree) {
            currentChain3->setNextChain(new ListChain(currentChain1->getMonomStruct()));
            resultList.realElements++;

            currentChain1 = currentChain1->getNextChain();
            currentChain3 = currentChain3->getNextChain();

            currentChain3->setNextChain(resultList.startChain);
         }
         else if (currentChain1->getMonomStruct().compoundDegree < currentChain2->getMonomStruct().compoundDegree) {
             currentChain3->setNextChain(new ListChain(currentChain2->getMonomStruct()));
             resultList.realElements++;

             currentChain2 = currentChain2->getNextChain();
             currentChain3 = currentChain3->getNextChain();

             currentChain3->setNextChain(resultList.startChain);
         }
         else {
             if ( currentChain1->getMonomStruct().factor + currentChain2->getMonomStruct().factor ) {
                 MonomStruct resultMonom(currentChain1->getMonomStruct());
                 resultMonom.factor += currentChain2->getMonomStruct().factor;

                 currentChain3->setNextChain(new ListChain(resultMonom));
                 resultList.realElements++;

                 currentChain1 = currentChain1->getNextChain();
                 currentChain2 = currentChain2->getNextChain();
                 currentChain3 = currentChain3->getNextChain();

                 currentChain3->setNextChain(resultList.startChain);
             }
             else {
                 currentChain1 = currentChain1->getNextChain();
                 currentChain2 = currentChain2->getNextChain();
             }
         }

    }

    return resultList;
}

List& List::operator=(List& list) {
    if (this == &list) {
        return *this;
    }

    for (int i = 0; i < list.getSizeList(); i++) {
        this->addToList(list.getMonomStruct(i));
    }
    this->realElements = list.realElements;

    return *this;
}
